#!/usr/local/bin/php
<?php

	for ($i = 0; $i <= $argc -1; ++$i) {
		
		$query_raw = preg_replace('/\s+/', '', strtoupper($argv[$i])); // REMOVE SPACE AND CONVERT STRING TO UPPERCASE
		$query = explode("=", $query_raw);
		$search_value = strtoupper($argv[$i]);
		$lowercase_state = str_replace("STATE=","", $search_value);
		$lowercase_area = str_replace("AREA=","", $search_value);
    }
	
	
	//GET CONTENT DATA FROM JSON FILE
	$str = file_get_contents('http://apims.doe.gov.my/data/public/CAQM/last24hours.json'); 
	
	//DECODE JSON DATA
	$json = json_decode($str, true);
	$val = $json['24hour_api'];
	
	//set array value for header
	$header = $json['24hour_api'][0];
	
	$date_today = date('d/m/Y');
	$date_yesterday = date('d/m/Y',strtotime("-1 days"));
	
	
	// CHECK IF USER WANT TO LIST STATE
	if($query[0] == "STATE"){
		
		print "Showing API for $lowercase_state  \nFROM $date_yesterday $header[2] UNTIL $date_today $header[25] \n";
		
		foreach($val as $value => $key){
			
			// GET ALL THE VALUE OF STATE FROM ARRAY
			$arr = $json['24hour_api'][$value][0];
			
			
			// REMOVE SPACE IN ARRAY VALUE
			$arr = preg_replace('/\s+/', '', $arr); 
			
			// COMPARE SEARCH VALUE WITH ARRAY VALUE
			if($arr == $query[1]){ 
				for($j = 1; $j <= 25; $j++){
					$data = $json['24hour_api'][$value][$j];
					print "$header[$j] -- $data \n";
				}
				print "------------------------------- \n";
			}
		}
		
	}else if($query[0] == "AREA"){ // CHECK IF USER WANT TO LIST AREA
		
		
		print "Showing API for $lowercase_area  \nFROM $date_yesterday $header[2] UNTIL $date_today $header[25] \n";
		
		foreach($val as $value => $key){
			
			// GET ALL THE VALUE OF STATE FROM ARRAY
			$arr = $json['24hour_api'][$value][1];
			
			
			// REMOVE SPACE IN ARRAY VALUE
			$arr = preg_replace('/\s+/', '', strtoupper($arr)); 
			
			// COMPARE SEARCH VALUE WITH ARRAY VALUE
			if($arr == $query[1]){ 
				for($j = 1; $j <= 25; $j++){
					$data = $json['24hour_api'][$value][$j];
					print "$header[$j] -- $data \n";
				}
				print "------------------------------- \n";
			}
		}
		
	}else{
		print'Command not found \n Please use this format :- \nphp show_api.php area="name of area" or php show_api.php state="name of state"';
	}
	
	
?>